/*
 * zadanie11.c
 *
 *  Created on: 25 mar 2022
 *      Author: Jozef
 */
#include "cwiczenie1.h"

#if ZADANIE == 11

#include <stdio.h>
#include <stdlib.h>
#include "funkcja2.h"
int main() {
	struct strukt elp;
	elp.np = NULL;
	elp.pop = NULL;
	elp.li_int = 48;
	elp.li_double = 3.1478;
	struct strukt *ell = &elp;
	for (int i = 0; i < 10; i++) {
		struct strukt *nele = (struct strukt*) malloc(sizeof(elp));
		nele->li_int = ell->li_int + i + 1;
		nele->li_double = ell->li_double + i + 1;
		ell->np = nele;
		nele->pop = ell;
		ell = nele;
	}
	ell->np = NULL;
	ell = &elp;
	struct strukt *el_pi = NULL;
	struct strukt *el_si = NULL;
	int i = 0;
	while (ell->np != NULL) {
		i++;
		printf("Element listy %d \n wartosc cał %d \n i rzeczywista %f \n", i,
				ell->li_int, ell->li_double);
		ell = ell->np;
		if (i == 5 - 1)
			el_pi = ell;
		if (i == 7 - 1)
			el_si = ell;
	}
	el_pi->np->pop = el_pi->pop;
	el_pi->pop->np = el_pi->np;
	el_si->np->pop = el_si->pop;
	el_si->pop->np = el_si->np;
	free(el_pi);
	free(el_si);
	ell = &elp;
	int d = 0;
	while (ell->np != NULL) {
		d++;
		printf("Element listy %d \n wartosc cał %d \n i rzeczywista %f \n", d,
				ell->li_int, ell->li_double);
		ell = ell->np;
	}
	return 0;
}
#endif
